
CREATE -- ALTER -- DROP
VIEW narita.effectiveStandings AS
	SELECT
		s.characterID,
		s.agentID,
		s.corporationID,
		s.factionID,
		s.standing + ((10 - s.standing) * (0.04 * CASE WHEN s.standing < 0 THEN COALESCE(sdip.level, 0) WHEN s.standing > 0 THEN COALESCE(scon.level, 0) ELSE 0 END)) AS standing
	FROM narita.standings AS s
	LEFT JOIN narita.characterSkills AS scon ON
		scon.characterID = s.characterID AND
		scon.typeID = 3359 -- Connections
	LEFT JOIN narita.characterSkills AS sdip ON
		sdip.characterID = s.characterID AND
		sdip.typeID = 3357; -- Diplomacy

GO



CREATE -- ALTER -- DROP
VIEW narita.invTypeMarketGroups AS
	WITH
		t AS (
			SELECT
				t.typeID,
				t.typeName,
				mg.marketGroupID,
				mg.marketGroupName,
				mg.parentGroupID
			FROM invTypes AS t
			INNER JOIN invMarketGroups AS mg ON mg.marketGroupID = t.marketGroupID
			UNION ALL
			SELECT
				t.typeID,
				t.typeName,
				mg.marketGroupID,
				mg.marketGroupName,
				mg.parentGroupID
			FROM t
			INNER JOIN invMarketGroups AS mg ON mg.marketGroupID = t.parentGroupID
		)
	SELECT t.*
	FROM t;

GO
