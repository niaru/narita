
CREATE -- ALTER -- DROP
VIEW narita.serverCacheStatus AS
	SELECT
		c.characterID,
		dt.dataType,
		ds.currentTime,
		COALESCE(ds.cachedUntil, DATEADD(MINUTE, -1, GETUTCDATE())) AS cachedUntil
	FROM narita.characters AS c
	CROSS JOIN (
		SELECT 'Character' AS dataType
		UNION ALL
		SELECT 'Standings'
		UNION ALL
		SELECT 'Assets'
		UNION ALL
		SELECT 'Wallet'
		UNION ALL
		SELECT 'Orders'
		UNION ALL
		SELECT 'Market'
	) AS dt
	LEFT JOIN (
		SELECT t.characterID, 'Character' AS dataType, MIN(t.currentTime) AS currentTime, MAX(t.cachedUntil) AS cachedUntil FROM narita.characters AS t GROUP BY t.characterID
		UNION ALL
		SELECT t.characterID, 'Standings', MIN(t.currentTime), MAX(t.cachedUntil) FROM narita.standings AS t GROUP BY t.characterID
		UNION ALL
		SELECT t.characterID, 'Assets', MIN(t.currentTime), MAX(t.cachedUntil) FROM narita.assets AS t GROUP BY t.characterID
		UNION ALL
		SELECT t.characterID, 'Wallet', MAX(t.currentTime), MAX(t.cachedUntil) FROM narita.wallet AS t GROUP BY t.characterID
		UNION ALL
		SELECT t.characterID, 'Orders', MIN(t.currentTime), MAX(t.cachedUntil) FROM narita.orders AS t GROUP BY t.characterID
		UNION ALL
		SELECT NULL, 'Market', MAX(t.date), CAST(CAST(DATEADD(DAY, 2, MAX(t.date)) AS varchar) + ' 00:15:00' AS smalldatetime) FROM narita.market AS t
	) AS ds ON
		COALESCE(ds.characterID, c.characterID) = c.characterID AND
		ds.dataType = dt.dataType;

GO



CREATE -- ALTER -- DROP
FUNCTION narita.serverReprocessingOrders(
	@characterID int = 90352180,
	@reprocessAtStationID int = 60003760,
	@sellAtStationID int = 60003760
)
RETURNS @result TABLE (
	rn bigint NOT NULL,
	typeID int NOT NULL,
	typeName nvarchar(100) NOT NULL,
	volRemaining int NOT NULL,
	volEntered int NOT NULL,
	volProgress tinyint NOT NULL,
	materialPrice money,
	price money NOT NULL,
	newPrice money,
	profit money,
	profitRatio int,
	materialPriceStatus varchar(10) NOT NULL,
	priceStatus varchar(10) NOT NULL,
	profitStatus varchar(10) NOT NULL,
	priority tinyint NOT NULL
) AS
BEGIN;
	DECLARE @marketDate date = (SELECT MAX(date) FROM narita.market);
	DECLARE @transactionTax decimal(4,3) = (SELECT transactionTax FROM narita.marketTransactionTax WHERE characterID = @characterID);
	DECLARE @eco TABLE (
		stationID int,
		orderID bigint,
		PRIMARY KEY (stationID, orderID)
	);
	INSERT INTO @eco
	SELECT
		t.stationID,
		eco.orderID
	FROM (
		SELECT DISTINCT o.stationID
		FROM narita.orders AS o
		WHERE
			o.characterID = @characterID AND
			o.bid = 1
	) AS t
	CROSS APPLY narita.marketEffectiveCacheOrders(t.stationID, 1) AS eco;
	INSERT INTO @result
	SELECT
		ROW_NUMBER() OVER(ORDER BY priority, REPLACE(t.typeName, '''', '"')) AS rn,
		t.typeID,
		t.typeName,
		t.volRemaining,
		t.volEntered,
		t.volProgress,
		t.materialPrice,
		t.price,
		t.newPrice,
		t.profit,
		t.profitRatio,
		CASE WHEN t.materialPrice < t.price OR t.materialPrice < t.newPrice THEN 'red' ELSE '' END AS materialPriceStatus,
		CASE WHEN t.price < t.newPrice THEN 'red' WHEN t.price > t.newPrice THEN 'orange' ELSE '' END AS priceStatus,
		CASE WHEN t.profitRatio < 104 THEN 'red' WHEN t.profitRatio < 107 THEN 'orange' ELSE '' END AS profitStatus,
		t.priority
	FROM (
		SELECT
			t.*,
			t.materialPrice - t.materialPriceTax - t.price AS profit,
			ROUND((t.materialPrice - t.materialPriceTax) / t.price, 2) * 100 AS profitRatio,
			CASE
				WHEN t.materialPrice < t.price THEN 1
				WHEN t.materialPrice < t.newPrice THEN 2
				WHEN t.price < t.newPrice THEN 3
				WHEN t.price > t.newPrice THEN 4
				ELSE 5
			END AS priority
		FROM (
			SELECT
				o.typeID,
				t.typeName,
				o.volRemaining,
				o.volEntered,
				FLOOR((1 - CAST(o.volRemaining AS decimal) / o.volEntered) * 100) AS volProgress,
				r.materialPrice / t.portionSize AS materialPrice,
				r.materialPrice / t.portionSize * @transactionTax AS materialPriceTax,
				o.price,
				CASE WHEN FLOOR(mc.bestPrice) + 1 != o.price THEN FLOOR(mc.bestPrice) + 1 END AS newPrice
			FROM narita.orders AS o
			INNER JOIN invTypes AS t ON t.typeID = o.typeID
			LEFT JOIN narita.reprocessingMaterialPrice(@characterID, @reprocessAtStationID, @sellAtStationID, @marketDate) AS r ON r.typeID = o.typeID
			LEFT JOIN (
				SELECT
					eco.stationID,
					mc.typeID,
					MAX(mc.price) AS bestPrice
				FROM narita.marketCache AS mc
				INNER JOIN @eco AS eco ON eco.orderID = mc.orderID
				LEFT JOIN narita.orders AS o ON
					o.characterID = @characterID AND
					o.orderID = mc.orderID
				WHERE
					o.orderID IS NULL AND
					mc.minVolume = 1
				GROUP BY
					eco.stationID,
					mc.typeID
			) AS mc ON
				mc.stationID = o.stationID AND
				mc.typeID = o.typeID
			WHERE
				o.characterID = @characterID AND
				o.bid = 1
		) AS t
	) AS t;
	RETURN;
END;

GO



CREATE -- ALTER -- DROP
FUNCTION narita.serverReprocessingMarket(
	@characterID int = 90352180,
	@buyAtStationID int = 60003760,
	@reprocessAtStationID int = 60003760,
	@sellAtStationID int = 60003760
)
RETURNS @result TABLE (
	typeID int NOT NULL,
	typeName nvarchar(100) NOT NULL,
	price money,
	materialPrice money,
	profit money,
	profitRatio int,
	volToOrder int,
	quality bigint,
	orderNo int,
	profitStatus varchar(10) NOT NULL
) AS
BEGIN;
	DECLARE @data TABLE (
		typeID int PRIMARY KEY,
		price money NOT NULL,
		materialPrice money NOT NULL,
		profit money NOT NULL,
		profitRatio int NOT NULL,
		orders int NOT NULL,
		avgVolume int NOT NULL,
		avgProfit money
	);
	INSERT INTO @data
	SELECT rb.*
	FROM narita.reprocessingMarket(@characterID, @buyAtStationID, @reprocessAtStationID, @sellAtStationID) AS rb;
	DECLARE @typesToIgnore TABLE (typeID int PRIMARY KEY);
	INSERT INTO @typesToIgnore
	SELECT tmg.typeID
	FROM narita.invTypeMarketGroups AS tmg
	WHERE tmg.marketGroupID IN (1035, 1111); -- Manufacture & Research / Components, Rigs
	DELETE FROM @data
	WHERE
		typeID IN (
			SELECT tti.typeID FROM @typesToIgnore AS tti
		);
	INSERT INTO @result
	SELECT
		t.typeID,
		t.typeName,
		t.price,
		t.materialPrice,
		t.profit,
		t.profitRatio,
		t.avgVolume,
		t.quality,
		CASE WHEN t.orderID IS NULL THEN NULL ELSE ROW_NUMBER() OVER(PARTITION BY CASE WHEN t.orderID IS NULL THEN 0 ELSE 1 END ORDER BY t.quality) END AS orderNo,
		CASE WHEN t.profitRatio < 104 THEN 'red' WHEN t.profitRatio < 107 THEN 'orange' ELSE '' END AS profitStatus
	FROM (
		SELECT
			COALESCE(d.typeID, ot.typeID) AS typeID,
			COALESCE(t.typeName, ot.typeName) AS typeName,
			d.price,
			d.materialPrice,
			d.profit,
			d.profitRatio,
			d.avgVolume * 3 AS avgVolume,
			d.profitRN + d.ordersRN + d.avgProfitRN AS quality,
			--d.profitRN + d.avgVolumeRN + d.ordersRN + d.avgProfitRN AS quality,
			o.orderID
		FROM (
			SELECT
				d.*,
				DENSE_RANK() OVER(ORDER BY d.profit * d.avgVolume DESC) AS profitRN,
				--DENSE_RANK() OVER(ORDER BY d.profit DESC) AS profitRN,
				--DENSE_RANK() OVER(ORDER BY d.avgVolume DESC) AS avgVolumeRN,
				DENSE_RANK() OVER(ORDER BY d.orders - CASE WHEN o.orderID IS NULL THEN 0 ELSE 1 END) AS ordersRN,
				DENSE_RANK() OVER(ORDER BY COALESCE(d.avgProfit, aap.avgAvgProfit) DESC) AS avgProfitRN,
				o.orderID
			FROM @data AS d
			CROSS JOIN (
				SELECT AVG(d.avgProfit) AS avgAvgProfit
				FROM @data AS d
			) AS aap
			LEFT JOIN narita.orders AS o ON
				o.characterID = @characterID AND
				o.bid = 1 AND
				o.typeID = d.typeID
			WHERE d.profitRatio > 104
		) AS d
		INNER JOIN invTypes AS t ON t.typeID = d.typeID
		FULL OUTER JOIN narita.orders AS o ON o.orderID = d.orderID
		LEFT JOIN invTypes AS ot ON ot.typeID = o.typeID
		WHERE
			o.characterID = @characterID OR
			o.characterID IS NULL
	) AS t;
	RETURN;
END;

GO



CREATE -- ALTER -- DROP
FUNCTION narita.serverMarketStatus()
RETURNS @result TABLE (
	typeID int,
	typeName nvarchar(100),
	price money,
	medianPrice money
) AS
BEGIN;
	DECLARE @marketDate date = (SELECT MAX(date) FROM narita.market);
	INSERT INTO @result
	SELECT
		t.typeID,
		t.typeName,
		t.price,
		mp.price
	FROM (
		SELECT
			m.typeID,
			t.typeName,
			MAX(m.price) AS price
		FROM invTypes AS t
		INNER JOIN narita.market AS m ON m.typeID = t.typeID
		WHERE
			t.marketGroupID = 18 AND -- Minerals
			m.date = @marketDate AND
			m.stationID = 60003760 AND
			m.bid = 1
		GROUP BY
			m.typeID,
			t.typeName
	) AS t
	INNER JOIN narita.marketMedianPrices(DEFAULT, 1, @marketDate, @marketDate) AS mp ON mp.typeID = t.typeID;
	RETURN;
END;

GO



CREATE -- ALTER -- DROP
FUNCTION narita.serverWealthChart(@characterID int)
RETURNS @result TABLE (
	label varchar(100) NOT NULL,
	value money NOT NULL
) AS
BEGIN;
	DECLARE @marketDate date = (SELECT MAX(date) FROM narita.market);
	INSERT INTO @result
	SELECT
		'Balance',
		c.balance
	FROM narita.characters AS c
	WHERE c.characterID = @characterID
	UNION ALL
	SELECT
		CASE WHEN o.bid = 1 THEN 'Buy' ELSE 'Sell' END,
		SUM(CASE WHEN o.bid = 1 THEN o.escrow ELSE o.price * o.volRemaining END)
	FROM narita.orders AS o
	WHERE o.characterID = @characterID
	GROUP BY o.bid
	UNION ALL
	SELECT
		'Assets',
		SUM(m.price * a.quantity)
	FROM narita.assets AS a
	INNER JOIN (
		SELECT
			m.typeID,
			MAX(m.price) AS price
		FROM narita.market AS m
		WHERE
			m.date = @marketDate AND
			m.stationID = 60003760 AND
			m.bid = 1
		GROUP BY m.typeID
	) AS m ON m.typeID = a.typeID
	WHERE a.characterID = @characterID
	HAVING SUM(m.price * a.quantity) IS NOT NULL;
	RETURN;
END;

GO



CREATE -- ALTER -- DROP
FUNCTION narita.serverProfitChart(@characterID int, @reprocessAtStationID int)
RETURNS @result TABLE (
	series varchar(20) NOT NULL,
	date date NOT NULL,
	value money NOT NULL
) AS
BEGIN;
	INSERT INTO @result
	SELECT
		'balance' AS series,
		CAST(w.transactionDateTime AS date) AS date,
		SUM(CASE WHEN w.transactionType = 's' THEN w.price * w.quantity ELSE 0 END) - SUM(CASE WHEN w.transactionType = 'b' THEN w.price * w.quantity ELSE 0 END) AS value
	FROM narita.wallet AS w
	inner join invTypes as t on t.typeID = w.typeID
	WHERE
		w.characterID = @characterID AND
		w.transactionDateTime >= DATEADD(MONTH, -1, GETUTCDATE())
	GROUP BY CAST(w.transactionDateTime AS date);
	/*
	INSERT INTO @result
	SELECT
		'projected' AS type,
		CAST(w.transactionDateTime AS date) AS date,
		SUM((r.materialPrice / t.portionSize * w.quantity) - (w.price * w.quantity)) AS value
	FROM narita.wallet AS w
	INNER JOIN invTypes AS t ON t.typeID = w.typeID
	INNER JOIN narita.reprocessingMaterialPrice(@characterID, @reprocessAtStationID, DEFAULT, w.tra) AS r ON r.typeID = w.typeID
	WHERE
		w.characterID = @characterID AND
		w.transactionType = 'b' AND
		w.transactionDateTime >= DATEADD(MONTH, -1, GETUTCDATE())
	GROUP BY CAST(w.transactionDateTime AS date);
	*/
	DECLARE @transactions TABLE (
		date date,
		typeID int,
		quantity int,
		totalPrice money
	);
	INSERT INTO @transactions
	SELECT
		CAST(w.transactionDateTime AS date) AS date,
		w.typeID,
		SUM(w.quantity) AS quantity,
		SUM(w.price * w.quantity) AS totalPrice
	FROM narita.wallet AS w
	INNER JOIN invTypes AS t ON t.typeID = w.typeID
	WHERE
		w.characterID = @characterID AND
		w.transactionType = 'b' AND
		w.transactionDateTime >= DATEADD(MONTH, -1, GETUTCDATE())
	GROUP BY
		CAST(w.transactionDateTime AS date),
		w.typeID;
	DECLARE @materialMedianPrices TABLE (
		date date,
		typeID int,
		price money
	);
	DECLARE @filter narita.marketCustomMedianPricesFilter;
	INSERT INTO @filter
	SELECT DISTINCT
		r.materialTypeID,
		DATEADD(DAY, -1, t.date),
		1
	FROM @transactions as t
	INNER HASH JOIN narita.reprocessing(@characterID, @reprocessAtStationID) AS r ON r.typeID = t.typeID;
	INSERT INTO @materialMedianPrices
	SELECT
		mp.date,
		mp.typeID,
		mp.price
	FROM narita.marketCustomMedianDayPrice(DEFAULT, @filter) AS mp;
	INSERT INTO @result
	SELECT
		'projected' AS series,
		t.date,
		SUM((mp.materialPrice / ty.portionSize * t.quantity) - totalPrice) AS value
	FROM @transactions AS t
	INNER JOIN invTypes AS ty ON ty.typeID = t.typeID
	INNER HASH JOIN (
		SELECT
			t.date,
			t.typeID,
			SUM(r.materialIReceive * mp.price) AS materialPrice
		FROM @transactions as t
		INNER HASH JOIN narita.reprocessing(@characterID, @reprocessAtStationID) AS r ON r.typeID = t.typeID
		INNER HASH JOIN @materialMedianPrices AS mp ON
			DATEADD(DAY, 1, mp.date) = t.date AND
			mp.typeID = r.materialTypeID
		GROUP BY
			t.date,
			t.typeID
	) AS mp ON
		mp.date = t.date AND
		mp.typeID = t.typeID
	GROUP BY t.date;
	RETURN;
END;

GO



CREATE -- ALTER -- DROP
FUNCTION narita.serverMarketPriceChart(@typeID int)
RETURNS @result TABLE (
	series varchar(10) NOT NULL,
	date smalldatetime NOT NULL,
	price money NOT NULL
) AS
BEGIN;
	DECLARE @marketDate date = (SELECT MAX(date) FROM narita.market);
	DECLARE @filter narita.marketCustomMedianPricesFilter;
	WITH
		t AS (
			SELECT @marketDate AS date
			UNION ALL
			SELECT DATEADD(DAY, -1, t.date)
			FROM t
			WHERE t.date > DATEADD(DAY, -6, @marketDate)
		)
	INSERT INTO @filter
	SELECT @typeID, t.date, NULL
	FROM t;
	INSERT INTO @result
	SELECT
		CASE WHEN mp.bid = 0 THEN 'sell' ELSE 'buy' END AS series,
		mp.date,
		mp.price
	FROM narita.marketCustomMedianDayPrice(DEFAULT, @filter) AS mp;
	RETURN;
END;

GO



CREATE -- ALTER -- DROP
VIEW narita.serverOrderPerformanceChart AS
	SELECT
		w.characterID,
		w.typeID,
		w.transactionDateTime,
		SUM(w.quantity) AS quantity
	FROM narita.wallet AS w
	WHERE
		w.transactionDateTime BETWEEN DATEADD(DAY, -1, GETUTCDATE()) AND GETUTCDATE() AND
		w.transactionType = 'b'
	GROUP BY
		w.characterID,
		w.typeID,
		w.transactionDateTime;

GO



CREATE -- ALTER -- DROP
FUNCTION narita.serverReprocessingProfitChart(
	@characterID int = 90352180,
	@buyAtStationID int = 60003760,
	@reprocessAtStationID int = 60003760,
	@sellAtStationID int = 60003760
)
RETURNS @result TABLE (
	typeID int NOT NULL,
	date date NOT NULL,
	profit money NOT NULL
) AS
BEGIN;
	DECLARE @marketDate date = (SELECT MAX(date) FROM narita.market);
	INSERT INTO @result
	SELECT
		rp.typeID,
		rp.date,
		rp.profit
	FROM narita.reprocessingProfit(@characterID, @buyAtStationID, @reprocessAtStationID, @sellAtStationID, DATEADD(DAY, -6, @marketDate), @marketDate) AS rp;
	RETURN;
END;

GO
