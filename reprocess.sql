
-- TODO OreSpecificProcessingSkillLevel
CREATE -- ALTER -- DROP
FUNCTION narita.reprocessingEffectiveYield(
	@characterID int = 90352180,
	@stationID int = 60003760
)
RETURNS decimal(9,7) AS
BEGIN
	RETURN (
		SELECT t.stationReprocessingEfficiency + (0.375 * (1 + (t.skillRefiningLevel * 0.02)) * (1 + (t.skillRefineryEfficiencyLevel * 0.04)) * (1 + (0 * 0.5)))
		FROM (
			SELECT
				(SELECT s.reprocessingEfficiency FROM staStations AS s WHERE s.stationID = @stationID) AS stationReprocessingEfficiency,
				COALESCE((SELECT s.level FROM narita.characterSkills AS s WHERE s.typeID = 3385 AND s.characterID = @characterID), 0) AS skillRefiningLevel,
				COALESCE((SELECT s.level FROM narita.characterSkills AS s WHERE s.typeID = 3389 AND s.characterID = @characterID), 0) AS skillRefineryEfficiencyLevel
		) AS t
	);
END;

GO



CREATE -- ALTER -- DROP
FUNCTION narita.reprocessingTax(
	@characterID int = 90352180,
	@stationID int = 60003760
)
RETURNS decimal(9,7) AS
BEGIN
	RETURN (
		SELECT CASE WHEN t.tax < 0 THEN 0 ELSE t.tax END
		FROM (
			SELECT (5 - (0.75 * COALESCE(es.standing, 0))) / 100 AS tax
			FROM staStations AS s
			LEFT JOIN narita.effectiveStandings AS es ON
				es.corporationID = s.corporationID AND
				es.characterID = @characterID
			WHERE s.stationID = @stationID
		) AS t
	);
END;

GO



CREATE -- ALTER -- DROP
FUNCTION narita.reprocessing(
	@characterID int = 90352180,
	@reprocessAtStationID int = 60003760
)
RETURNS @result TABLE (
	typeID int,
	materialTypeID int,
	materialQuantity int,
	materialYield int,
	materialTax int,
	materialIReceive int,
	materialTheyTake int,
	materialUnrecoverable int
) AS
BEGIN;
	DECLARE @effectiveYield decimal(9,7) = (SELECT narita.reprocessingEffectiveYield(@characterID, @reprocessAtStationID));
	DECLARE @reprocessingTax decimal(9,7) = (SELECT narita.reprocessingTax(@characterID, @reprocessAtStationID));
	INSERT INTO @result
	SELECT
		t.typeID,
		t.materialTypeID,
		t.materialQuantity,
		ROUND(t.materialYield, 0) AS materialYield,
		ROUND(t.materialTax, 0) AS materialTax,
		ROUND(t.materialYield - t.materialTax, 0) AS materialIReceive,
		ROUND(t.materialTax, 0) AS materialTheyTake,
		ROUND(t.materialQuantity - t.materialYield, 0) AS materialUnrecoverable
	FROM (
		SELECT
			tm.typeID,
			tm.materialTypeID,
			tm.quantity AS materialQuantity,
			tm.quantity * @effectiveYield AS materialYield,
			tm.quantity * @effectiveYield * @reprocessingTax AS materialTax
		FROM invTypeMaterials AS tm
	) AS t;
	RETURN;
END;

GO



CREATE -- ALTER -- DROP
FUNCTION narita.reprocessingMaterialPrice(
	@characterID int = 90352180,
	@reprocessAtStationID int = 60003760,
	@sellAtStationID int = 60003760,
	@date date
)
RETURNS TABLE AS
RETURN (
	SELECT
		r.typeID,
		SUM(r.materialIReceive * mp.price) AS materialPrice
	FROM narita.reprocessing(@characterID, @reprocessAtStationID) AS r
	INNER HASH JOIN narita.marketMedianPrices(@sellAtStationID, 1, @date, @date) AS mp ON mp.typeID = r.materialTypeID
	GROUP BY r.typeID
);

GO


CREATE -- ALTER -- DROP
FUNCTION narita.reprocessingProfit(
	@characterID int = 90352180,
	@buyAtStationID int = 60003760,
	@reprocessAtStationID int = 60003760,
	@sellAtStationID int = 60003760,
	@fromDate date,
	@tillDate date
)
RETURNS @result TABLE (
	typeID int,
	date date,
	profit money NOT NULL,
	PRIMARY KEY (typeID, date)
) AS
BEGIN;
	DECLARE @transactionTax decimal(4,3) = (SELECT transactionTax FROM narita.marketTransactionTax WHERE characterID = @characterID);
	DECLARE @brokerFee decimal(22,20) = (SELECT brokerFee FROM narita.marketBrokerFee WHERE characterID = @characterID AND stationID = @buyAtStationID);
	DECLARE @data TABLE (
		date date NOT NULL,
		typeID int NOT NULL,
		quantity int NOT NULL,
		price money NOT NULL,
		PRIMARY KEY (date, typeID)
	);
	INSERT INTO @data
	SELECT
		CAST(w.transactionDateTime AS date) AS date,
		w.typeID,
		SUM(w.quantity) AS quantity,
		SUM(w.price * w.quantity) AS price
	FROM narita.wallet AS w
	WHERE
		w.characterID = @characterID AND
		w.stationID = @buyAtStationID AND
		w.transactionType = 'b' AND
		CAST(w.transactionDateTime AS date) BETWEEN @fromDate AND @tillDate
	GROUP BY
		CAST(w.transactionDateTime AS date),
		w.typeID;
	DECLARE @filter narita.marketCustomMedianPricesFilter;
	INSERT INTO @filter
	SELECT DISTINCT
		r.materialTypeID,
		d.date,
		1
	FROM narita.reprocessing(@characterID, @reprocessAtStationID) AS r
	INNER JOIN @data AS d ON d.typeID = r.typeID;
	INSERT INTO @result
	SELECT
		d.typeID,
		d.date,
		(r.materialPrice - r.materialPrice * @transactionTax) * d.quantity - (d.price - d.price * @brokerFee) AS profit
	FROM @data AS d
	INNER JOIN (
		SELECT
			r.typeID,
			mp.date,
			SUM(mp.price * r.materialIReceive) AS materialPrice
		FROM narita.reprocessing(@characterID, @reprocessAtStationID) AS r
		INNER JOIN narita.marketCustomMedianDayPrice(@sellAtStationID, @filter) AS mp ON mp.typeID = r.materialTypeID
		GROUP BY
			r.typeID,
			mp.date
	) AS r ON
		r.typeID = d.typeID AND
		r.date = d.date;
	RETURN;
END;

GO



CREATE -- ALTER -- DROP
FUNCTION narita.reprocessingMarket(
	@characterID int = 90352180,
	@buyAtStationID int = 60003760,
	@reprocessAtStationID int = 60003760,
	@sellAtStationID int = 60003760
)
RETURNS @result TABLE (
	typeID int PRIMARY KEY,
	price money NOT NULL,
	materialPrice money NOT NULL,
	profit money NOT NULL,
	profitRatio int NOT NULL,
	orders int NOT NULL,
	avgVolume int NOT NULL,
	avgProfit money
) AS
BEGIN;
	DECLARE @marketDate date = (SELECT MAX(date) FROM narita.market);
	DECLARE @transactionTax decimal(4,3) = (SELECT transactionTax FROM narita.marketTransactionTax WHERE characterID = @characterID);
	DECLARE @brokerFee decimal(22,20) = (SELECT brokerFee FROM narita.marketBrokerFee WHERE characterID = @characterID AND stationID = @buyAtStationID);
	DECLARE @volume TABLE (
		typeID int,
		volume int
		PRIMARY KEY (typeID, volume)
	);
	INSERT INTO @volume
	SELECT
		av.typeID,
		av.volume
	FROM narita.marketAvgVolMovedPerDay(@buyAtStationID, 1, DATEADD(DAY, -6, @marketDate), @marketDate) AS av
	WHERE av.volume > 1;
	DECLARE @market TABLE (
		typeID int,
		price money,
		orders int,
		PRIMARY KEY (typeID, price, orders)
	);
	INSERT INTO @market
	SELECT
		m.typeID,
		MAX(m.price) AS price,
		COUNT(m.orderID) AS orders
	FROM narita.market AS m
	INNER JOIN @volume AS v ON v.typeID = m.typeID
	INNER JOIN narita.marketLatestEffectiveOrders(@buyAtStationID, 1) AS eo ON
		eo.date = m.date AND
		eo.orderID = m.orderID
	WHERE
		DATEADD(DAY, m.duration, m.issued) > GETUTCDATE() AND
		--m.minVolume < 1000
		m.minVolume = 1 -- ?
	GROUP BY m.typeID;
	DECLARE @profit TABLE (
		typeID int,
		profit money,
		PRIMARY KEY(typeID, profit)
	);
	INSERT INTO @profit
	SELECT
		rp.typeID,
		AVG(rp.profit) AS profit
	FROM narita.reprocessingProfit(@characterID, @buyAtStationID, @reprocessAtStationID, @sellAtStationID, DATEADD(DAY, -6, @marketDate), @marketDate) AS rp
	GROUP BY rp.typeID;
	INSERT INTO @result
	SELECT
		t.typeID,
		t.price,
		t.materialPrice,
		(t.materialPrice - t.materialPriceTax) - (t.price - t.priceTax) AS profit,
		ROUND((t.materialPrice - t.materialPriceTax) / (t.price - t.priceTax), 2) * 100 AS profitRatio,
		t.orders,
		t.avgVolume,
		t.avgProfit
	FROM (
		SELECT
			r.typeID,
			m.price,
			m.price * @brokerFee AS priceTax,
			r.materialPrice / t.portionSize AS materialPrice,
			r.materialPrice / t.portionSize * @transactionTax AS materialPriceTax,
			m.orders,
			v.volume AS avgVolume,
			p.profit AS avgProfit
		FROM narita.reprocessingMaterialPrice(@characterID, @reprocessAtStationID, @sellAtStationID, @marketDate) AS r
		INNER JOIN invTypes AS t ON t.typeID = r.typeID
		INNER JOIN @market AS m ON
			m.typeID = r.typeID AND
			m.price * t.portionSize < r.materialPrice
		INNER JOIN @volume AS v ON v.typeID = r.typeID
		LEFT JOIN @profit AS p ON p.typeID = r.typeID
	) AS t;
	RETURN;
END;

GO
