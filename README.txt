﻿Narita is a tool for finding profitable market orders for reprocessing and refining and keeping those orders updated in EVE Online. It works by pulling in data from EVE Online API, EVE Online client and EVE-Central, analysing it and showing you the most profitable orders in the station you currently are docked at. It also can check your orders on the market, to determine whether you need to update the price. Here's how it looks: http://imgur.com/a/0znLi.

Market data comes from nightly EVE-Central [1] dumbs, updatable once a day, Consider running Contribtastic [2]. Your orders come from EVE Online client ("Export" button in your order tab), updatable as often as you need. Market price check data comes from EVE Online client cache, thanks to libevecache [3], updatable as often as you need. All other data comes from EVE Online API, updatable based on cache timers.

[1] http://eve-central.com/
[2] http://eve-central.com/home/software.html
[3] http://dev.eve-central.com/libevecache/
