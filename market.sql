
CREATE -- ALTER -- DROP
VIEW narita.marketTransactionTax AS
	SELECT
		c.characterID,
		(1 - 0.1 * COALESCE(s.level, 0)) / 100 AS transactionTax
	FROM narita.characters AS c
	LEFT JOIN narita.characterSkills AS s ON
		s.characterID = c.characterID AND
		s.typeID = 16622; -- Accounting

GO



CREATE -- ALTER -- DROP
VIEW narita.marketBrokerFee AS
	SELECT
		t.characterID,
		t.stationID,
		((1 - 0.05 * t.skillBrokerRelationsLevel) / POWER(2.0, 0.14 * t.factionStanding + 0.06 * t.corporationStanding) / 100) AS brokerFee
	FROM (
		SELECT
			c.characterID,
			s.stationID,
			COALESCE(cs.level, 0) AS skillBrokerRelationsLevel,
			COALESCE(ces.standing, 0) AS corporationStanding,
			COALESCE(fes.standing, 0) AS factionStanding
		FROM narita.characters AS c
		CROSS JOIN staStations AS s
		LEFT JOIN narita.characterSkills AS cs ON
			cs.characterID = c.characterID AND
			cs.typeID = 3446 -- Broker Relations
		LEFT JOIN (
			SELECT
				s.characterID,
				st.stationID,
				s.standing
			FROM narita.standings AS s
			INNER JOIN staStations AS st ON st.corporationID = s.corporationID
		) AS ces ON
			ces.characterID = c.characterID AND
			ces.stationID = s.stationID
		LEFT JOIN (
			SELECT
				s.characterID,
				st.stationID,
				s.standing
			FROM narita.standings AS s
			INNER JOIN crpNPCCorporations AS c ON c.factionID = s.factionID
			INNER JOIN staStations AS st ON st.corporationID = c.corporationID
		) AS fes ON
			fes.characterID = c.characterID AND
			fes.stationID = s.stationID
	) AS t;

GO



CREATE -- ALTER -- DROP
FUNCTION narita.marketMedianPrices(
	@stationID int = 60003760,
	@bid bit = 1,
	@fromDate date,
	@tillDate date
)
RETURNS TABLE AS
RETURN (
	SELECT
		t.typeID,
		AVG(t.price) AS price
	FROM (
		SELECT
			m.typeID,
			m.price,
			ROW_NUMBER() OVER(PARTITION BY m.typeID ORDER BY m.price) AS rn,
			COUNT(m.typeID) OVER(PARTITION BY m.typeID) AS c
		FROM narita.market AS m
		WHERE
			m.date BETWEEN @fromDate AND @tillDate AND
			m.stationID = @stationID AND
			m.bid = @bid
	) AS t
	WHERE t.rn IN (ROUND(CAST(t.c AS decimal) / 2, 0), CASE WHEN t.c % 2 = 0 THEN t.c / 2 + 1 ELSE -1 END)
	GROUP BY t.typeID
);

GO



CREATE -- DROP
TYPE narita.marketCustomMedianPricesFilter AS TABLE (typeID int, date date, bid bit);

GO

CREATE -- ALTER -- DROP
FUNCTION narita.marketCustomMedianDayPrice(
	@stationID int = 60003760,
	@filter marketCustomMedianPricesFilter READONLY
)
RETURNS TABLE AS
RETURN (
	SELECT
		t.typeID,
		t.date,
		t.bid,
		AVG(t.price) AS price
	FROM (
		SELECT
			m.typeID,
			m.date,
			m.bid,
			m.price,
			ROW_NUMBER() OVER(PARTITION BY m.typeID, m.date, m.bid ORDER BY m.price) AS rn,
			COUNT(m.typeID) OVER(PARTITION BY m.typeID, m.date, m.bid) AS c
		FROM narita.market AS m
		INNER JOIN @filter AS f ON
			f.typeID = m.typeID AND
			f.date = m.date AND
			COALESCE(f.bid, m.bid) = m.bid
		WHERE m.stationID = @stationID
	) AS t
	WHERE t.rn IN (ROUND(CAST(t.c AS decimal) / 2, 0), CASE WHEN t.c % 2 = 0 THEN t.c / 2 + 1 ELSE -1 END)
	GROUP BY
		t.typeID,
		t.date,
		t.bid
);

GO



CREATE -- ALTER -- DROP
FUNCTION narita.marketEffectiveOrders(
	@stationID int = 60003760,
	@bid bit = 1,
	@fromDate date,
	@tillDate date
)
RETURNS @result TABLE (
	date date,
	orderID bigint,
	PRIMARY KEY (date, orderID)
) AS
BEGIN;
	DECLARE @regionID int, @solarSystemID int;
	SELECT
		@regionID = regionID,
		@solarSystemID = solarSystemID
	FROM staStations
	WHERE stationID = @stationID;
	INSERT INTO @result
	SELECT
		m.date,
		m.orderID
	FROM narita.market AS m
	INNER JOIN staStations AS s ON
		s.stationID = m.stationID AND
		s.regionID = @regionID
	INNER JOIN narita.marketSolarSystemRange AS ssr ON
		ssr.fromSolarSystemID = @solarSystemID AND
		ssr.toSolarSystemID = s.solarSystemID
	WHERE
		m.date BETWEEN @fromDate AND @tillDate AND
		m.bid = @bid AND ((
				m.range = 32767 -- region
			) OR (
				m.range IN (-1, 65535) AND -- station
				m.stationID = @stationID
			) OR (
				m.range BETWEEN 0 AND 40 AND -- solar system or range
				m.range >= ssr.jumps
		));
	RETURN;
END;

GO



CREATE -- ALTER -- DROP
FUNCTION narita.marketLatestEffectiveOrders(
	@stationID int = 60003760,
	@bid bit = 1
)
RETURNS @result TABLE (
	date date,
	orderID bigint,
	PRIMARY KEY (date DESC, orderID)
) AS
BEGIN;
	DECLARE @regionID int, @solarSystemID int;
	SELECT
		@regionID = regionID,
		@solarSystemID = solarSystemID
	FROM staStations
	WHERE stationID = @stationID;
	INSERT INTO @result
	SELECT
		t.date,
		t.orderID
	FROM (
		SELECT
			m.date,
			m.orderID,
			DENSE_RANK() OVER(PARTITION BY m.typeID ORDER BY m.date DESC, m.reportedTime DESC) AS r
		FROM narita.market AS m
		INNER JOIN staStations AS s ON
			s.stationID = m.stationID AND
			s.regionID = @regionID
		INNER JOIN narita.marketSolarSystemRange AS ssr ON
			ssr.fromSolarSystemID = @solarSystemID AND
			ssr.toSolarSystemID = s.solarSystemID
		WHERE
			m.bid = @bid AND ((
					m.range = 32767 -- region
				) OR (
					m.range IN (-1, 65535) AND -- station
					m.stationID = @stationID
				) OR (
					m.range BETWEEN 0 AND 40 AND -- solar system or range
					m.range >= ssr.jumps
			))
	) AS t
	WHERE t.r = 1;
	RETURN;
END;

GO



CREATE -- ALTER -- DROP
FUNCTION narita.marketEffectiveCacheOrders(
	@stationID int = 60003760,
	@bid bit = 1
)
RETURNS @result TABLE (
	orderID bigint PRIMARY KEY
) AS
BEGIN;
	DECLARE @regionID int, @solarSystemID int;
	SELECT
		@regionID = regionID,
		@solarSystemID = solarSystemID
	FROM staStations
	WHERE stationID = @stationID;
	INSERT INTO @result
	SELECT m.orderID
	FROM narita.marketCache AS m
	INNER JOIN staStations AS s ON
		s.stationID = m.stationID AND
		s.regionID = @regionID
	INNER JOIN narita.marketSolarSystemRange AS ssr ON
		ssr.fromSolarSystemID = @solarSystemID AND
		ssr.toSolarSystemID = s.solarSystemID
	WHERE
		m.bid = @bid AND ((
				m.range = 32767 -- region
			) OR (
				m.range IN (-1, 65535) AND -- station
				m.stationID = @stationID
			) OR (
				m.range BETWEEN 0 AND 40 AND -- solar system or range
				m.range >= ssr.jumps
		));
	RETURN;
END;

GO



CREATE -- ALTER -- DROP
FUNCTION narita.marketAvgVolMovedPerDay(
	@stationID int = 60003760,
	@bid bit = 1,
	@fromDate date,
	@tillDate date
)
RETURNS @result TABLE (
	typeID int PRIMARY KEY,
	volume int
) AS
BEGIN;
	DECLARE @m TABLE (
		orderID bigint NOT NULL,
		rn bigint NOT NULL,
		date date NOT NULL,
		typeID int NOT NULL,
		volRemaining int NOT NULL,
		PRIMARY KEY (orderID, rn)
	);
	INSERT INTO @m
	SELECT
		m.orderID,
		ROW_NUMBER() OVER(PARTITION BY m.orderID ORDER BY m.date DESC) AS rn,
		m.date,
		m.typeID,
		m.volRemaining
	FROM narita.market AS m
	INNER JOIN narita.marketEffectiveOrders(@stationID, @bid, @fromDate, @tillDate) AS eo ON
		eo.date = m.date AND
		eo.orderID = m.orderID
	INSERT INTO @result
	SELECT
		t.typeID,
		AVG(t.volume) AS volume
	FROM (
		SELECT
			m1.date,
			m1.typeID,
			COALESCE(SUM(CAST(m2.volRemaining - m1.volRemaining AS bigint) / DATEDIFF(DAY, m2.date, m1.date)), 0) AS volume
		FROM @m AS m1
		LEFT JOIN @m AS m2 ON
			m2.orderID = m1.orderID AND
			m2.rn = m1.rn + 1
		GROUP BY
			m1.date,
			m1.typeID
	) AS t
	GROUP BY t.typeID;
	RETURN;
END;

GO
