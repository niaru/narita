﻿using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Web.UI.DataVisualization.Charting;
using System;

namespace Narita {

	class MarketPriceChart : Chart {

		public MarketPriceChart(Int32 typeID) : base() {
			this.chart.Height = 16;
			this.chart.Width = 28;
			this.chart.Series.Add(
				new Series("sell") { ChartType = SeriesChartType.Line }
			);
			this.chart.Series.Add(
				new Series("buy") { ChartType = SeriesChartType.Line }
			);
			this.addPoints(typeID);
		}

		protected void addPoints(Int32 typeID) {
			using (var connection = new SqlConnection(this.connectionString)) {
				connection.Open();
				var command = connection.CreateCommand();
				command.CommandText = @"
					SELECT *
					FROM narita.serverMarketPriceChart(@typeID);
				";
				command.Parameters.Add("@typeID", SqlDbType.Int).Value = typeID;
				var reader = command.ExecuteReader();
				while (reader.Read()) {
					this.chart.Series[reader["series"].ToString()].Points.AddXY(reader["date"], reader["price"]);
				}
			}
		}

	}

}
