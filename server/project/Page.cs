﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Xml;
using System;

namespace Narita {

	class Page {

		protected String apiRoot = "https://api.eveonline.com/";

		protected String connectionString = @"Server=PANDEMONA\SQLEXPRESS; Database=ccp_db; Trusted_Connection=True";

		protected HttpListenerRequest request;

		protected HttpListenerResponse response;

		public CultureInfo Culture;

		public Page(HttpListenerRequest request, HttpListenerResponse response) {
			this.request = request;
			this.response = response;
		}

		protected Dictionary<String, String> getApiCredentials(Int32 characterID) {
			using (var connection = new SqlConnection(this.connectionString)) {
				connection.Open();
				var command = connection.CreateCommand();
				command.CommandText = @"
					SELECT keyID, vCode
					FROM narita.characters
					WHERE characterID = @characterID;
				";
				command.Parameters.Add("@characterID", SqlDbType.Int).Value = characterID;
				var reader = command.ExecuteReader();
				if (!reader.Read()) {
					return null;
				}
				else {
					var result = new Dictionary<String, String>();
					result.Add("keyID", reader["keyID"].ToString());
					result.Add("vCode", reader["vCode"].ToString());
					return result;
				}
			}
		}

		protected String formatDbValue(String typeName, Object value) {
			switch (typeName) {
				case "tinyint":
				case "int":
				case "bigint":
				case "varchar":
				case "nvarchar":
					return value.ToString();
				case "money":
					return DBNull.Value.Equals(value) ? "" : ((decimal)value).ToString("C", this.Culture);
				case "smalldatetime":
				case "datetime":
					return DBNull.Value.Equals(value) ? "" : ((DateTime)value).ToString();
				default:
					throw new NotSupportedException(typeName);
			}
		}

		protected String formatDbValueAsAge(Object date) {
			if (DBNull.Value.Equals(date)) {
				return "";
			}
			else {
				var tmp = DateTime.UtcNow - (DateTime)date;
				return (new TimeSpan(tmp.Days, tmp.Hours, tmp.Minutes, tmp.Seconds)).ToString();
			}
		}

		public virtual void Process(XmlWriter doc) {
		}

	}

}
