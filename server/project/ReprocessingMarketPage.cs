﻿using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Xml;
using System;

namespace Narita {

	class ReprocessingMarketPage : Page {

		public ReprocessingMarketPage(HttpListenerRequest request, HttpListenerResponse response) : base(request, response) {
		}

		protected void write(XmlWriter xml) {
			xml.WriteStartElement("orders");
			using (var connection = new SqlConnection(this.connectionString)) {
				connection.Open();
				var command = connection.CreateCommand();
				command.CommandTimeout = 300;
				command.CommandText = @"
					SELECT *
					FROM narita.serverReprocessingMarket(@characterID, @stationID, @stationID, @stationID)
					ORDER BY quality;
				";
				command.Parameters.Add("@characterID", SqlDbType.Int).Value = this.request.Headers["EVE_CHARID"];
				command.Parameters.Add("@stationID", SqlDbType.Int).Value = this.request.Headers["EVE_STATIONID"];
				var reader = command.ExecuteReader();
				while (reader.Read()) {
					xml.WriteStartElement("order");
					for (var i = 0; i < reader.VisibleFieldCount; i++) {
						xml.WriteAttributeString(reader.GetName(i), this.formatDbValue(reader.GetDataTypeName(i), reader.GetValue(i)));
					}
					xml.WriteEndElement();
				}
			}
			xml.WriteEndElement();
		}

		public override void Process(XmlWriter xml) {
			this.write(xml);
		}

	}

}
