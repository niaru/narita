﻿using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.IO.Compression;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml;
using System;

namespace Narita {

	class HomePage : Page {

		public HomePage(HttpListenerRequest request, HttpListenerResponse response) : base(request, response) {
		}

		protected void update(String data) {
			switch (data) {
				case "Character":
					this.updateFromApi("CharacterSheet.xml.aspx", "narita.updateCharacters");
					break;
				case "Standings":
					this.updateFromApi("Standings.xml.aspx", "narita.updateStandings");
					break;
				case "Assets":
					this.updateFromApi("AssetList.xml.aspx", "narita.updateAssets");
					break;
				case "Orders":
					//this.updateFromApi("MarketOrders.xml.aspx", "narita.updateOrdersAPI");
					this.updateOrdersFromFile();
					break;
				case "Wallet":
					this.updateWalletFromApi();
					break;
				case "Market":
					this.updateFromEveCentral();
					break;
				default:
					throw new NotSupportedException(this.request.Url.ToString());
			}
		}

		protected void updateFromApi(String remoteFile, String sqlProcedure) {
			var characterID = Int32.Parse(this.request.Headers["EVE_CHARID"]);
			var credentials = this.getApiCredentials(characterID);
			var localFile = Path.GetTempFileName();
			var client = new WebClient();
			client.QueryString.Add("characterID", characterID.ToString());
			client.QueryString.Add("keyID", credentials["keyID"]);
			client.QueryString.Add("vCode", credentials["vCode"]);
			try {
				client.DownloadFile(String.Concat(this.apiRoot, "char/", remoteFile), localFile);
			}
			catch (WebException exception) {
				Debug.WriteLine(new StreamReader(exception.Response.GetResponseStream()).ReadToEnd());
				throw exception;
			}
			using (var connection = new SqlConnection(this.connectionString)) {
				connection.Open();
				var command = connection.CreateCommand();
				command.CommandText = String.Format("EXEC {0} {1}, '{2}';", sqlProcedure, characterID, localFile);
				command.ExecuteNonQuery();
			}
			File.Delete(localFile);
			this.response.Redirect("/");
		}

		protected void updateWalletFromApi() {
			var characterID = Int32.Parse(this.request.Headers["EVE_CHARID"]);
			var credentials = this.getApiCredentials(characterID);
			var remoteFile = "WalletTransactions.xml.aspx";
			var localFile = Path.GetTempFileName();
			var requestRowCount = 2560;
			using (var connection = new SqlConnection(this.connectionString)) {
				connection.Open();
				var transaction = connection.BeginTransaction();
				var walletRowCountCommand = connection.CreateCommand();
				walletRowCountCommand.Transaction = transaction;
				walletRowCountCommand.CommandText = @"
					SELECT COUNT(*)
					FROM narita.wallet;
				";
				var currentRows = (Int32)walletRowCountCommand.ExecuteScalar();
				var client = new WebClient();
				client.QueryString.Add("characterID", characterID.ToString());
				client.QueryString.Add("keyID", credentials["keyID"]);
				client.QueryString.Add("vCode", credentials["vCode"]);
				client.QueryString.Add("rowCount", requestRowCount.ToString());
				try {
					client.DownloadFile(String.Concat(this.apiRoot, "char/", remoteFile), localFile);
				}
				catch (WebException exception) {
					Debug.WriteLine(new StreamReader(exception.Response.GetResponseStream()).ReadToEnd());
					throw exception;
				}
				var command = connection.CreateCommand();
				command.Transaction = transaction;
				command.CommandText = String.Format("EXEC narita.updateWallet {0}, '{1}';", characterID, localFile);
				command.ExecuteNonQuery();
				var insertedRows = (Int32)walletRowCountCommand.ExecuteScalar() - currentRows;
				while (insertedRows == requestRowCount) {
					currentRows += insertedRows;
					command = connection.CreateCommand();
					command.Transaction = transaction;
					command.CommandText = @"
						SELECT MIN(transactionID)
						FROM narita.wallet
						WHERE
							currentTime = (
								SELECT MAX(currentTime) FROM narita.wallet
							);
					";
					var currentLastTransactionID = command.ExecuteScalar().ToString();
					client.QueryString.Remove("beforeTransID");
					client.QueryString.Add("beforeTransID", currentLastTransactionID);
					try {
						client.DownloadFile(String.Concat(this.apiRoot, "char/", remoteFile), localFile);
					}
					catch (WebException exception) {
						Debug.WriteLine(new StreamReader(exception.Response.GetResponseStream()).ReadToEnd());
						throw exception;
					}
					command = connection.CreateCommand();
					command.Transaction = transaction;
					command.CommandText = String.Format("EXEC narita.updateWallet {0}, '{1}';", characterID, localFile);
					command.ExecuteNonQuery();
					insertedRows = (Int32)walletRowCountCommand.ExecuteScalar() - currentRows;
				}
				transaction.Commit();
			}
			File.Delete(localFile);
			this.response.Redirect("/");
		}

		protected void updateOrdersFromFile() {
			var characterID = this.request.Headers["EVE_CHARID"];
			var path = @"C:\Users\crt\Documents\EVE\logs\Marketlogs\";
			var files = Directory.GetFiles(path).ToList();
			if (files.Count > 0) {
				var lastFile = files.OrderBy(file => File.GetLastWriteTimeUtc(file)).Last();
				using (var connection = new SqlConnection(this.connectionString)) {
					connection.Open();
					var command = connection.CreateCommand();
					command.CommandText = String.Format("EXEC narita.updateOrdersFile {0}, '{1}';", characterID, lastFile);
					command.ExecuteNonQuery();
				}
				File.Delete(lastFile);
			}
			this.response.Redirect("/");
		}

		protected void updateFromEveCentral() {
			var path = @"C:\Users\crt\Desktop\narita\data\eve-central\";
			Directory.SetCurrentDirectory(path);
			var newestFileDate = DateTime.UtcNow.AddDays(-1).Date;
			var lastFileDate = DateTime.UtcNow.AddDays(-2).Date;
			var files = Directory.GetFiles(path).ToList();
			if (files.Count > 0) {
				files.Sort();
				var lastFile = Path.GetFileNameWithoutExtension(files.Last());
				var lastFileDateParts = lastFile.Split('-');
				lastFileDate = new DateTime(Int32.Parse(lastFileDateParts[0]), Int32.Parse(lastFileDateParts[1]), Int32.Parse(lastFileDateParts[2]));
			}
			while (newestFileDate > lastFileDate) {
				lastFileDate = lastFileDate.AddDays(1);
				var remoteFile = String.Concat(lastFileDate.ToString("yyyy-MM-dd"), ".dump.gz");
				var tempFile = remoteFile;
				var localFile = Regex.Replace(tempFile, @"\.gz$", "");
				var client = new WebClient();
				try {
					client.DownloadFile(String.Concat("http://eve-central.com/dumps/", remoteFile), tempFile);
				}
				catch (WebException exception) {
					Debug.WriteLine(new StreamReader(exception.Response.GetResponseStream()).ReadToEnd());
					throw exception;
				}
				var isGZipFile = false;
				using (var stream = File.OpenRead(tempFile)) {
					var header = new byte[2];
					stream.Read(header, 0, 2);
					isGZipFile = header[0] == 31 && header[1] == 139;
				}
				if (isGZipFile) {
					using (var inFile = File.OpenRead(tempFile)) {
						using (var gzStream = new GZipStream(inFile, CompressionMode.Decompress)) {
							using (var outFile = File.Create(localFile)) {
								gzStream.CopyTo(outFile);
							}
						}
					}
				}
				File.Delete(tempFile);
			}
			using (var connection = new SqlConnection(this.connectionString)) {
				connection.Open();
				var command = connection.CreateCommand();
				command.CommandText = @"
					SELECT MAX(date)
					FROM narita.market;
				";
				var lastDbFileObj = command.ExecuteScalar();
				var lastDbFileDate = DBNull.Value.Equals(lastDbFileObj) ? DateTime.UtcNow.AddDays(-2).Date : (DateTime)lastDbFileObj;
				foreach (var file in Directory.GetFiles(path)) {
					var fileDateParts = Path.GetFileNameWithoutExtension(file).Split('-');
					var fileDate = new DateTime(Int32.Parse(fileDateParts[0]),  Int32.Parse(fileDateParts[1]), Int32.Parse(fileDateParts[2]));
					if (fileDate > lastDbFileDate) {
						command = connection.CreateCommand();
						command.CommandTimeout = 600;
						command.CommandText = String.Format("EXEC narita.updateMarket '{0}';", file);
						command.ExecuteNonQuery();
					}
				}
			}
			this.response.Redirect("/");
		}

		protected void write(XmlWriter doc) {
			using (var connection = new SqlConnection(this.connectionString)) {
				connection.Open();
				var command = connection.CreateCommand();
				command.CommandText = @"
					SELECT *
					FROM narita.serverCacheStatus
					WHERE characterID = @characterID OR characterID IS NULL;
				";
				command.Parameters.Add("@characterID", SqlDbType.Int).Value = this.request.Headers["EVE_CHARID"];
				var reader = command.ExecuteReader();
				doc.WriteStartElement("cacheStatus");
				while (reader.Read()) {
					doc.WriteStartElement("data");
					for (var i = 0; i < reader.VisibleFieldCount; i++) {
						doc.WriteAttributeString(reader.GetName(i), this.formatDbValue(reader.GetDataTypeName(i), reader.GetValue(i)));
					}
					doc.WriteAttributeString("age", this.formatDbValueAsAge(reader["currentTime"]));
					doc.WriteAttributeString("updateAvailableIn", this.formatDbValueAsAge(reader["cachedUntil"]));
					doc.WriteAttributeString("updateAvailable", (DateTime.UtcNow > (DateTime)reader["cachedUntil"]).ToString());
					doc.WriteEndElement();
				}
				doc.WriteEndElement();
				reader.Close();
				doc.WriteStartElement("wealthStatus");
				doc.WriteEndElement();
				command = connection.CreateCommand();
				command.CommandText = @"
					SELECT *
					FROM narita.serverMarketStatus()
					ORDER BY typeName;
				";
				reader = command.ExecuteReader();
				doc.WriteStartElement("marketStatus");
				while (reader.Read()) {
					doc.WriteStartElement("type");
					for (var i = 0; i < reader.VisibleFieldCount; i++) {
						doc.WriteAttributeString(reader.GetName(i), this.formatDbValue(reader.GetDataTypeName(i), reader.GetValue(i)));
					}
					doc.WriteEndElement();
				}
				doc.WriteEndElement();
				doc.WriteStartElement("profitStatus");
				doc.WriteEndElement();
			}
		}

		public override void Process(XmlWriter xml) {
			if (request.QueryString["update"] != null) {
				this.update(this.request.QueryString["update"]);
			}
			else {
				this.write(xml);
			}
		}

	}

}
