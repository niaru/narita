﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Web.UI.DataVisualization.Charting;
using System;

namespace Narita {

	class OrderPerformanceChart : Chart {

		protected static Dictionary<String, KeyValuePair<DateTime, DataTable>> data = new Dictionary<String, KeyValuePair<DateTime, DataTable>>();

		public OrderPerformanceChart(Int32 characterID, Int32 typeID) {
			this.chart.Height = 16;
			this.chart.Width = 48;
			this.area.AxisX.MajorGrid.Enabled = true;
			this.area.AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
			this.area.AxisX.MajorGrid.Interval = 6;
			this.area.AxisX.Minimum = DateTime.UtcNow.AddDays(-1).ToOADate();
			this.area.AxisX.Maximum = DateTime.UtcNow.ToOADate();
			this.chart.Series.Add(
				new Series("quantity") {
					ChartType = SeriesChartType.FastPoint,
					MarkerSize = 1,
					Color = ColorTranslator.FromHtml("#AFAFAF"),
					XValueMember = "transactionDateTime",
					YValueMembers = "quantity"
				}
			);
			lock (typeof(OrderPerformanceChart)) {
				this.loadData(characterID);
			}
			var key = characterID.ToString();
			var view = new DataView(OrderPerformanceChart.data[key].Value);
			view.RowFilter = String.Format("typeID = '{0}'", typeID);
			this.chart.DataSource = view;
			this.chart.DataBind();
		}

		protected void loadData(Int32 characterID) {
			using (var connection = new SqlConnection(this.connectionString)) {
				connection.Open();
				var command = connection.CreateCommand();
				command.CommandText = @"
					SELECT COALESCE(MAX(currentTime), GETUTCDATE())
					FROM narita.wallet
					WHERE characterID = @characterID;
				";
				command.Parameters.Add("@characterID", SqlDbType.Int).Value = characterID;
				var date = (DateTime)command.ExecuteScalar();
				var key = characterID.ToString();
				if (!OrderPerformanceChart.data.ContainsKey(key) || OrderPerformanceChart.data[key].Key < date) {
					command = connection.CreateCommand();
					command.CommandText = @"
						SELECT *
						FROM narita.serverOrderPerformanceChart
						WHERE characterID = @characterID;
					";
					command.Parameters.Add("@characterID", SqlDbType.Int).Value = characterID;
					var data = new DataTable();
					data.Load(command.ExecuteReader());
					var kvp = new KeyValuePair<DateTime, DataTable>(date, data);
					OrderPerformanceChart.data[key] = kvp;
				}
			}
		}

	}

}
