﻿using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Globalization;
using System.Threading;
using System.Web.UI.DataVisualization.Charting;
using System;

namespace Narita {

	class ProfitChart : Chart {

		public ProfitChart(CultureInfo culture, Int32 characterID, Int32 stationID) : base() {
			Thread.CurrentThread.CurrentCulture = culture;
			Thread.CurrentThread.CurrentUICulture = culture;
			this.area.Position = new ElementPosition() { Width = 100F, Height = 100F };
			this.chart.Height = 130;
			this.chart.Width = 400;
			this.chart.AntiAliasing = AntiAliasingStyles.Graphics;
			this.area.AxisY.MajorGrid.Enabled = true;
			this.area.AxisY.MajorGrid.Interval = 100000000;
			this.area.AxisY.IsStartedFromZero = true;
			this.area.AxisY.LabelStyle.Enabled = true;
			this.area.AxisY.LabelStyle.Format = "{c}";
			this.area.AxisY.LabelStyle.Interval = 100000000;
			this.area.AxisX.MajorGrid.Enabled = true;
			this.area.AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Days;
			this.area.AxisX.MajorGrid.Interval = 1;
			this.area.AxisX.LabelStyle.Enabled = true;
			this.area.AxisX.LabelStyle.Format = "{dd}";
			this.area.AxisX.LabelStyle.Interval = 1;
			this.area.AxisX.LabelStyle.IsStaggered = true;
			this.chart.Series.Add(
				new Series("projected") { ChartType = SeriesChartType.Spline }
			);
			this.chart.Series.Add(
				new Series("balance") { ChartType = SeriesChartType.Spline }
			);
			this.loadData(characterID, stationID);
		}

		protected void loadData(Int32 characterID, Int32 stationID) {
			using (var connection = new SqlConnection(this.connectionString)) {
				connection.Open();
				var command = connection.CreateCommand();
				command.CommandText = @"
					SELECT *
					FROM narita.serverProfitChart(@characterID, @stationID)
					WHERE series = 'projected'
					ORDER BY date;
				";
				command.Parameters.Add("@characterID", SqlDbType.Int).Value = characterID;
				command.Parameters.Add("@stationID", SqlDbType.Int).Value = stationID;
				var reader = command.ExecuteReader();
				while (reader.Read()) {
					this.chart.Series[reader["series"].ToString()].Points.AddXY(reader["date"], reader["value"]);
				}
			}
		}

	}

}
