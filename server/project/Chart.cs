﻿using Charting = System.Web.UI.DataVisualization.Charting;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Web.UI.DataVisualization.Charting;
using System;

namespace Narita {

	class Chart {

		protected Charting.Chart chart = new Charting.Chart();
		
		protected ChartArea area = new ChartArea();

		protected String connectionString = @"Server=PANDEMONA\SQLEXPRESS; Database=ccp_db; Trusted_Connection=True;";

		public Chart() {
			this.chart.AntiAliasing = AntiAliasingStyles.None;
			this.chart.BorderWidth = 0;
			this.chart.BorderlineDashStyle = ChartDashStyle.Solid;
			this.chart.BorderlineWidth = 2;
			this.chart.BorderlineColor = ColorTranslator.FromHtml("#282828");
			this.chart.BackColor = Color.Transparent;
			this.area.BorderWidth = 0;
			this.area.BackColor = Color.Transparent;
			this.area.AxisX.LabelStyle.Enabled = false;
			this.area.AxisY.LabelStyle.Enabled = false;
			this.area.AxisX.LabelStyle.ForeColor = ColorTranslator.FromHtml("#AFAFAF");
			this.area.AxisY.LabelStyle.ForeColor = ColorTranslator.FromHtml("#AFAFAF");
			this.area.AxisX.IsLabelAutoFit = false;
			this.area.AxisY.IsLabelAutoFit = false;
			this.area.AxisX.MajorGrid.LineWidth = 1;
			this.area.AxisY.MajorGrid.LineWidth = 1;
			this.area.AxisX.MajorGrid.LineColor = ColorTranslator.FromHtml("#282828");
			this.area.AxisY.MajorGrid.LineColor = ColorTranslator.FromHtml("#282828");
			this.area.AxisX.MajorGrid.Enabled = false;
			this.area.AxisY.MajorGrid.Enabled = false;
			this.area.AxisX.MajorTickMark.Enabled = false;
			this.area.AxisY.MajorTickMark.Enabled = false;
			this.area.AxisX.LineWidth = 0;
			this.area.AxisY.LineWidth = 0;
			this.area.AxisX.IsMarginVisible = false;
			this.area.AxisY.IsMarginVisible = false;
			this.area.AxisX.IsMarksNextToAxis = false;
			this.area.AxisY.IsMarksNextToAxis = false;
			this.area.AxisX.IsStartedFromZero = false;
			this.area.AxisY.IsStartedFromZero = false;
			this.chart.ChartAreas.Add(area);
		}

		public virtual void Save(Stream outStream) {
			this.chart.SaveImage(outStream, ChartImageFormat.Png);
		}

	}

}
