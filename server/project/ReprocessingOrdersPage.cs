﻿using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml;
using System;

namespace Narita {

	class ReprocessingOrdersPage : Page {

		protected String cachePath = @"C:\Users\crt\AppData\Local\CCP\EVE\d_eve_tranquility\cache\MachoNet\87.237.38.200\299\CachedMethodCalls";

		protected String dumperPath = @"C:\Users\crt\Desktop\narita\server\dumper.exe";

		public ReprocessingOrdersPage(HttpListenerRequest request, HttpListenerResponse response) : base(request, response) {
		}

		protected void update() {
			using (var connection = new SqlConnection(this.connectionString)) {
				connection.Open();
				var command = connection.CreateCommand();
				command.CommandText = @"
					SELECT MAX(reportedTime)
					FROM narita.marketCache;
				";
				var lastFileDateObject = command.ExecuteScalar();
				var lastFileDate = DBNull.Value.Equals(lastFileDateObject) ? DateTime.UtcNow.AddHours(-1) : (DateTime)lastFileDateObject;
				var gotNewData = false;
				var outFile = Path.GetTempFileName();
				using (var outStream = File.CreateText(outFile)) {
					foreach (var inFile in Directory.GetFiles(this.cachePath)) {
						if (File.GetLastWriteTimeUtc(inFile) > lastFileDate) {
							var dumper = new Process();
							dumper.StartInfo.FileName = this.dumperPath;
							dumper.StartInfo.Arguments = String.Concat("--market ", inFile);
							dumper.StartInfo.CreateNoWindow = true;
							dumper.StartInfo.UseShellExecute = false;
							dumper.StartInfo.RedirectStandardOutput = true;
							dumper.Start();
							while (!dumper.StandardOutput.EndOfStream) {
								var line = dumper.StandardOutput.ReadLine();
								if (Regex.IsMatch(line, @"^\d+")) {
									outStream.WriteLine(line);
								}
							}
							gotNewData = true;
						}
					}
				}
				if (gotNewData) {
					command = connection.CreateCommand();
					command.CommandText = String.Format("EXEC narita.updateMarketCache '{0}';", outFile);
					command.ExecuteNonQuery();
					File.Delete(outFile);
				}
				if (!String.IsNullOrEmpty(this.request.QueryString["checkedTypeIDs"])) {
					command = connection.CreateCommand();
					command.CommandText = @"
						MERGE narita.marketCacheUpdates AS t
						USING (
							SELECT @stationID AS stationID, @typeID AS typeID
						) AS s ON
							s.stationID = t.stationID AND
							s.typeID = t.typeID
						WHEN MATCHED THEN UPDATE SET t.updateTime = GETUTCDATE()
						WHEN NOT MATCHED THEN INSERT VALUES (s.stationID, s.typeID, GETUTCDATE());
					";
					command.Parameters.Add("@stationID", SqlDbType.Int).Value = this.request.Headers["EVE_STATIONID"];
					command.Parameters.Add("@typeID", SqlDbType.Int);
					foreach (var typeID in this.request.QueryString["checkedTypeIDs"].Split(',')) {
						command.Parameters["@typeID"].Value = typeID;
						command.ExecuteNonQuery();
					}
				}
			}
			var continu = this.request.QueryString["continue"] != null ? "?continue=" : "";
			response.Redirect(String.Concat("/reprocessingOrders", continu));
		}

		protected void write(XmlWriter xml) {
			using (var connection = new SqlConnection(this.connectionString)) {
				connection.Open();
				xml.WriteStartElement("orders");
				var command = connection.CreateCommand();
				command.CommandText = @"
					SELECT MIN(mcu.updateTime)
					FROM narita.orders AS o
					INNER JOIN narita.marketCacheUpdates AS mcu ON
						mcu.stationID = o.stationID AND
						mcu.typeID = o.typeID
					WHERE o.characterID = @characterID;
				";
				command.Parameters.Add("@characterID", SqlDbType.Int).Value = this.request.Headers["EVE_CHARID"];
				var lastUpdate = this.formatDbValueAsAge(command.ExecuteScalar());
				xml.WriteAttributeString("lastUpdate", lastUpdate);
				command = connection.CreateCommand();
				command.CommandText = @"
					SELECT *
					FROM narita.serverReprocessingOrders(@characterID, @stationID, @stationID)
					ORDER BY rn;
				";
				command.Parameters.Add("@characterID", SqlDbType.Int).Value = this.request.Headers["EVE_CHARID"];
				command.Parameters.Add("@stationID", SqlDbType.Int).Value = this.request.Headers["EVE_STATIONID"];
				var reader = command.ExecuteReader();
				while (reader.Read()) {
					xml.WriteStartElement("order");
					for (var i = 0; i < reader.VisibleFieldCount; i++) {
						xml.WriteAttributeString(reader.GetName(i), this.formatDbValue(reader.GetDataTypeName(i), reader.GetValue(i)));
					}
					xml.WriteEndElement();
				}
				xml.WriteEndElement();
				reader.Close();
				command = connection.CreateCommand();
				command.CommandText = @"
					SELECT o.typeID
					FROM narita.orders AS o
					LEFT JOIN narita.marketCacheUpdates AS mcu ON
						mcu.stationID = o.stationID AND
						mcu.typeID = o.typeID
					WHERE
						o.characterID = @characterID AND
						o.stationID = @stationID AND (
							mcu.typeID IS NULL OR
							DATEADD(MINUTE, 30, mcu.updateTime) < GETUTCDATE()
						)
					ORDER BY mcu.updateTime, NEWID();
				";
				command.Parameters.Add("@characterID", SqlDbType.Int).Value = this.request.Headers["EVE_CHARID"];
				command.Parameters.Add("@stationID", SqlDbType.Int).Value = this.request.Headers["EVE_STATIONID"];
				reader = command.ExecuteReader();
				xml.WriteStartElement("oldOrders");
				while (reader.Read()) {
					xml.WriteStartElement("order");
					for (var i = 0; i < reader.VisibleFieldCount; i++) {
						xml.WriteAttributeString(reader.GetName(i), this.formatDbValue(reader.GetDataTypeName(i), reader.GetValue(i)));
					}
					xml.WriteEndElement();
				}
				xml.WriteEndElement();
				reader.Close();
			}
		}

		public override void Process(XmlWriter xml) {
			if (this.request.QueryString["update"] != null) {
				this.update();
			}
			else {
				this.write(xml);
			}
		}

	}

}
