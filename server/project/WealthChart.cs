﻿using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Globalization;
using System.Threading;
using System.Web.UI.DataVisualization.Charting;
using System;
using System.Linq;

namespace Narita {

	class WealthChart : Chart {

		public WealthChart(CultureInfo culture, Int32 characterID) : base() {
			Thread.CurrentThread.CurrentCulture = culture;
			Thread.CurrentThread.CurrentUICulture = culture;
			this.chart.Height = 175;
			this.chart.Width = 175;
			this.chart.AntiAliasing = AntiAliasingStyles.Graphics;
			var series = new Series("wealth") {
				ChartType = SeriesChartType.Pie,
				LabelForeColor = ColorTranslator.FromHtml("#AFAFAF"),
				XValueMember = "label",
				YValueMembers = "value",
				LegendText = "#VALY{c}"
			};
			series["PieLabelStyle"] = "Outside";
			series["PieLineColor"] = "#AFAFAF";
			this.chart.Series.Add(series);
			var legend = new Legend() {
				BackColor = Color.Transparent,
				ForeColor = ColorTranslator.FromHtml("#AFAFAF"),
				Docking = Docking.Bottom
			};
			this.chart.Legends.Add(legend);
			this.loadData(characterID);
		}

		protected void loadData(Int32 characterID) {
			using (var connection = new SqlConnection(this.connectionString)) {
				connection.Open();
				var command = connection.CreateCommand();
				command.CommandText = @"
					SELECT *
					FROM narita.serverWealthChart(@characterID);
				";
				command.Parameters.Add("@characterID", SqlDbType.Int).Value = characterID;
				var data = new DataTable();
				data.Load(command.ExecuteReader());
				this.chart.DataSource = data;
				this.chart.DataBind();
				var total = data.Select().Sum(row => Double.Parse(row["value"].ToString()));
				this.chart.Legends[0].CustomItems.Add(Color.White, String.Format("{0:c}", total));
			}
		}

	}

}
