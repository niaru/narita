﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Web.UI.DataVisualization.Charting;
using System;

namespace Narita {

	class ReprocessingProfitChart : Chart {

		protected static Dictionary<String, KeyValuePair<DateTime, DataTable>> data = new Dictionary<String, KeyValuePair<DateTime, DataTable>>();

		public ReprocessingProfitChart(Int32 characterID, Int32 stationID, Int32 typeID) {
			this.chart.Height = 16;
			this.chart.Width = 30;
			this.area.AxisX.IntervalType = DateTimeIntervalType.Days;
			this.area.AxisX.Interval = 1;
			this.area.AxisX.Minimum = DateTime.UtcNow.AddDays(-8).ToOADate();
			this.area.AxisX.Maximum = DateTime.UtcNow.AddDays(-1).ToOADate();
			var series = new Series("profit") {
				ChartType = SeriesChartType.Column,
				Color = ColorTranslator.FromHtml("#AFAFAF"),
				XValueMember = "date",
				YValueMembers = "profit"
			};
			series["PixelPointWidth"] = "1";
			this.chart.Series.Add(series);
			lock (typeof(ReprocessingProfitChart)) {
				this.loadData(characterID, stationID);
			}
			var key = String.Format("{0}:{1}", characterID, stationID);
			var view = new DataView(ReprocessingProfitChart.data[key].Value);
			view.RowFilter = String.Format("typeID = '{0}'", typeID);
			this.chart.DataSource = view;
			this.chart.DataBind();
		}

		protected void loadData(Int32 characterID, Int32 stationID) {
			using (var connection = new SqlConnection(this.connectionString)) {
				connection.Open();
				var command = connection.CreateCommand();
				command.CommandText = @"
					SELECT COALESCE(MAX(currentTime), GETUTCDATE())
					FROM narita.wallet
					WHERE characterID = @characterID;
				";
				command.Parameters.Add("@characterID", SqlDbType.Int).Value = characterID;
				var date = (DateTime)command.ExecuteScalar();
				var key = String.Format("{0}:{1}", characterID, stationID);
				if (!ReprocessingProfitChart.data.ContainsKey(key) || ReprocessingProfitChart.data[key].Key < date) {
					command = connection.CreateCommand();
					command.CommandText = @"
						SELECT *
						FROM narita.serverReprocessingProfitChart(@characterID, @stationID, @stationID, @stationID) AS rp;
					";
					command.Parameters.Add("@characterID", SqlDbType.Int).Value = characterID;
					command.Parameters.Add("@stationID", SqlDbType.Int).Value = stationID;
					var data = new DataTable();
					data.Load(command.ExecuteReader());
					var kvp = new KeyValuePair<DateTime, DataTable>(date, data);
					ReprocessingProfitChart.data[key] = kvp;
				}
			}
		}

	}

}
