﻿using System.Diagnostics;
using System.IO;
using System.Net;
using System.Xml;
using System;
using System.Globalization;

namespace Narita {

	class Server {

		protected String root;

		protected String connectionString = @"Server=PANDEMONA\SQLEXPRESS; Database=ccp_db; Trusted_Connection=True;";

		protected CultureInfo culture;

		public Server(String prefix, String root) {
			this.root = root;
			this.culture = new CultureInfo("en-US");
			this.culture.NumberFormat.CurrencySymbol = "ISK ";
			this.culture.NumberFormat.CurrencyPositivePattern = 3;
			this.culture.NumberFormat.CurrencyNegativePattern = 8;
			var http = new HttpListener();
			http.Prefixes.Add(prefix);
			http.Start();
			while (http.IsListening) {
				var callback = new AsyncCallback(this.dispatch);
				var result = http.BeginGetContext(callback, http);
				result.AsyncWaitHandle.WaitOne();
			}
		}

		protected void dispatch(IAsyncResult result) {
			var http = (HttpListener)result.AsyncState;
			var context = http.EndGetContext(result);
			var request = context.Request;
			var response = context.Response;
			using (var responseStream = new MemoryStream()) {
				var localPath = Path.Combine(this.root, request.Url.LocalPath.Substring(1));
				if (File.Exists(localPath)) {
					using (var stream = File.OpenRead(localPath)) {
						stream.CopyTo(responseStream);
					}
				}
				else if (request.Url.AbsolutePath.StartsWith("/chart/")) {
					Chart chart;
					var characterID = Int32.Parse(request.Headers["EVE_CHARID"]);
					var stationID = Int32.Parse(request.Headers["EVE_STATIONID"]);
					var typeID = -1;
					if (!String.IsNullOrEmpty(request.QueryString["typeID"])) {
						typeID = Int32.Parse(request.QueryString["typeID"]);
					}
					switch (request.Url.AbsolutePath) {
						case "/chart/marketPrice.png":
							chart = new MarketPriceChart(typeID);
							break;
						case "/chart/wealth.png":
							chart = new WealthChart(this.culture, characterID);
							break;
						case "/chart/profit.png":
							chart = new ProfitChart(this.culture, characterID, stationID);
							break;
						case "/chart/orderPerformance.png":
							chart = new OrderPerformanceChart(characterID, typeID);
							break;
						case "/chart/reprocessingProfit.png":
							chart = new ReprocessingProfitChart(characterID, stationID, typeID);
							break;
						default:
							throw new NotSupportedException(request.Url.ToString());
					}
					chart.Save(responseStream);
					response.ContentType = "image/png";
				}
				else {
					var doc = new XmlDocument();
					using (var xml = doc.CreateNavigator().AppendChild()) {
						String xslFile = null;
						Page page = null;
						switch (request.Url.AbsolutePath) {
							case "/":
								xslFile = "home";
								page = new HomePage(request, response);
								break;
							case "/reprocessingOrders":
								xslFile = "reprocessingOrders";
								page = new ReprocessingOrdersPage(request, response);
								break;
							case "/reprocessingMarket":
								xslFile = "reprocessingMarket";
								page = new ReprocessingMarketPage(request, response);
								break;
						}
						if (page != null) {
							page.Culture = this.culture;
							var stylesheet = String.Format(@"type=""text/xsl"" href=""/{0}.xsl""", xslFile);
							xml.WriteProcessingInstruction(@"xml-stylesheet", stylesheet);
							xml.WriteStartElement("page");
							page.Process(xml);
							xml.WriteEndElement();
							xml.Close();
							using (var writer = XmlWriter.Create(responseStream)) {
								doc.Save(writer);
							}
							response.ContentType = "application/xml";
						}
					}
				}
				response.ContentLength64 = responseStream.Length;
				responseStream.WriteTo(response.OutputStream);
			}
		}

	}

}
