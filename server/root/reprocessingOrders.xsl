<?xml version="1.0" encoding="utf-8"?>
<t:stylesheet version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:t="http://www.w3.org/1999/XSL/Transform">
	<t:include href="/templates.xsl" />
	<t:output method="xml" indent="no" media-type="application/xhtml+xml" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" />

	<t:template match="/page">
		<html xml:lang="en">
			<t:call-template name="head">
				<t:with-param name="title">Reprocessing Orders</t:with-param>
			</t:call-template>
			<body id="reprocessingOrders">
				<t:call-template name="menu" />
				<div id="content">
					<t:apply-templates />
				</div>
			</body>
		</html>
	</t:template>

	<t:template match="oldOrders">
		<script type="text/javascript">
			var typeIDs = [<t:for-each select="order"><t:value-of select="@typeID" /><t:if test="position() != last()">, </t:if></t:for-each>];
			Ext.onReady(function() {
				if (location.search == '?continue=') {
					updateOrdersFromCache(typeIDs);
				}
			});
		</script>
	</t:template>

	<t:template match="orders">
		<div class="dataWrap">
			<table class="data">
				<caption>
					<span>Reprocessing Orders</span>
					<span class="meta"><span>&#x2014;</span>Age: <t:value-of select="@lastUpdate" /></span>
					<span class="actions">
						<a href="#" onclick="updateOrdersFromCache(typeIDs); return false;">Update</a>
					</span>
				</caption>
				<thead>
					<tr>
						<th>I</th>
						<th>Type</th>
						<th>Quantity</th>
						<th>History</th>
						<th>Material Price</th>
						<th>Price</th>
						<th>Profit</th>
						<th>New Price</th>
					</tr>
				</thead>
				<tbody>
					<t:for-each select="order">
						<tr>
							<td class="icon">
								<a href="#" onclick="CCPEVE.showMarketDetails({@typeID}); return false;">
									<img class="icon" src="http://image.eveonline.com/Type/{@typeID}_32.png" />
								</a>
							</td>
							<td><t:value-of select="@typeName" /></td>
							<td class="progress">
								<div class="progressWrap">
									<div class="progressValue" style="width:{@volProgress}%;">
										<div class="progressText">
											<t:value-of select="@volRemaining" /> / <t:value-of select="@volEntered" />
										</div>
									</div>
								</div>
							</td>
							<td class="spark"><img src="/chart/orderPerformance.png?typeID={@typeID}" /></td>
							<td class="{@materialPriceStatus}"><t:value-of select="@materialPrice" /></td>
							<td class="{@priceStatus}"><t:value-of select="@price" /></td>
							<td>
								<t:if test="@profitRatio != ''">
									<span class="{@profitStatus}"><t:value-of select="@profitRatio" />%</span> &#8756; <t:value-of select="@profit" />
								</t:if>
							</td>
							<td><t:value-of select="@newPrice" /></td>
						</tr>
					</t:for-each>
				</tbody>
			</table>
		</div>
		<div class="dataClear"></div>
	</t:template>

</t:stylesheet>
