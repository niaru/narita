<?xml version="1.0" encoding="utf-8"?>
<t:stylesheet version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:t="http://www.w3.org/1999/XSL/Transform">
	<t:include href="/templates.xsl" />
	<t:output method="xml" indent="no" media-type="application/xhtml+xml" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" />

	<t:template match="cacheStatus">
		<div class="dataWrap">
			<table class="data">
				<caption>API Status</caption>
				<thead>
					<tr>
						<th>Data</th>
						<th>Age</th>
						<th>Update</th>
					</tr>
				</thead>
				<tbody>
					<t:for-each select="data">
						<tr>
							<td><t:value-of select="@dataType" /></td>
							<td><t:value-of select="@age" /></td>
							<td>
								<t:choose>
									<t:when test="@updateAvailable = 'True'">
										<a href="#" onclick="location.href = location.pathname + '?update={@dataType}';">Update</a>
									</t:when>
									<t:otherwise>
										<t:value-of select="@updateAvailableIn" />
									</t:otherwise>
								</t:choose>
							</td>
						</tr>
					</t:for-each>
				</tbody>
			</table>
		</div>
		<div class="dataClear"></div>
	</t:template>

	<t:template match="wealthStatus">
		<div class="dataWrap">
			<div class="data">
				<div class="caption">Wealth</div>
				<div class="content">
					<img src="/chart/wealth.png" />
				</div>
			</div>
		</div>
		<div class="dataClear"></div>
	</t:template>

	<t:template match="marketStatus">
		<div class="dataWrap">
			<table class="data">
				<caption>Market</caption>
				<thead>
					<tr>
						<th>I</th>
						<th>Type</th>
						<th>Price</th>
						<th>Median Price</th>
						<th>H</th>
					</tr>
				</thead>
				<tbody>
					<t:for-each select="type">
						<tr>
							<td class="icon">
								<a href="#" onclick="CCPEVE.showMarketDetails({@typeID}); return false;">
									<img class="icon" src="http://image.eveonline.com/Type/{@typeID}_32.png" />
								</a>
							</td>
							<td><t:value-of select="@typeName" /></td>
							<td><t:value-of select="@price" /></td>
							<td><t:value-of select="@medianPrice" /></td>
							<td class="spark">
								<img src="/chart/marketPrice.png?typeID={@typeID}" />
							</td>
						</tr>
					</t:for-each>
				</tbody>
			</table>
		</div>
		<div class="dataClear"></div>
	</t:template>

	<t:template match="profitStatus">
		<div class="dataWrap">
			<div class="data">
				<div class="caption">Profit</div>
				<div class="content">
					<img src="/chart/profit.png" />
				</div>
			</div>
		</div>
		<div class="dataClear"></div>
	</t:template>

	<t:template match="/page">
		<html xml:lang="en">
			<t:call-template name="head">
				<t:with-param name="title">Home</t:with-param>
			</t:call-template>
			<body id="home">
				<t:call-template name="menu" />
				<div id="content">
					<table>
						<tr>
							<td valign="top"><t:apply-templates select="marketStatus" /></td>
							<td valign="top"><t:apply-templates select="wealthStatus" /></td>
							<td valign="top"><t:apply-templates select="cacheStatus" /></td>
						</tr>
					</table>
					<table>
						<tr>
							<td valign="top"><t:apply-templates select="profitStatus" /></td>
						</tr>
					</table>
				</div>
			</body>
		</html>
	</t:template>

</t:stylesheet>
