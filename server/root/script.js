﻿
CCPEVE.requestTrust('http://localhost:8000/');

var updateOrdersFromCache = function(typeIDs) {
	updateNextOrderFromCache(typeIDs, 0);
};

var updateNextOrderFromCache = function(typeIDs, index) {
	var getCheckedTypeIDs = function() {
		var checkedTypeIDs = '';
		for (var i = 0; i < index; i++) {
			if (i == 0) {
				checkedTypeIDs = typeIDs[i];
			}
			else {
				checkedTypeIDs += ',' + typeIDs[i];
			}
		}
		return checkedTypeIDs;
	};
	if (index >= typeIDs.length) {
		var checkedTypeIDs = getCheckedTypeIDs();
		location.href = location.pathname + '?update=&checkedTypeIDs=' + checkedTypeIDs;
	}
	else if (index >= 30) {
		var checkedTypeIDs = getCheckedTypeIDs();
		location.href = location.pathname + '?update=&checkedTypeIDs=' + checkedTypeIDs + '&continue=';
	}
	else {
		CCPEVE.showMarketDetails(typeIDs[index]);
		setTimeout(function() { updateNextOrderFromCache(typeIDs, ++index); }, 2700);
	}
};

Ext.onReady(function() {
	Ext.getBody().on({
		'dblclick': function(event, target) {
			var parent = target.parentElement;
			if (parent.tagName == 'tr') {
				Ext.select('tr.highlight').each(function(row) {
					Ext.get(row).removeClass('highlight')
				});
				Ext.get(parent).addClass('highlight');
			}
		}
	});
});
