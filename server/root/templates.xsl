﻿<?xml version="1.0" encoding="utf-8"?>
<t:stylesheet version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:t="http://www.w3.org/1999/XSL/Transform">
	<t:output method="xml" indent="no" media-type="application/xhtml+xml" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" />

	<t:template name="head">
		<t:param name="title" />
		<head>
			<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
			<title><t:value-of select="$title" /></title>
			<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.3.0/build/cssreset/reset-min.css" />
			<link rel="stylesheet" type="text/css" href="/style.css" />
			<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/ext-core/3.1.0/ext-core.js"></script>
			<script type="text/javascript" src="/script.js"></script>
		</head>
	</t:template>

	<t:template name="menu">
		<ul id="menu">
			<li><a id="homeNav" href="/">Home</a></li>
			<li>
				<a id="reprocessingNav">Reprocessing &#x25BC;</a>
				<ul class="subMenu">
					<li><a id="reprocessingOrdersNav" href="/reprocessingOrders">Orders</a></li>
					<li><a id="reprocessingMarketNav" href="/reprocessingMarket">Market</a></li>
					<!--<li><a id="reprocessingAssetsNav" href="/reprocessingAssets">Assets</a></li>-->
				</ul>
			</li>
		</ul>
	</t:template>

</t:stylesheet>
