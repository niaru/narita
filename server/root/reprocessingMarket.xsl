<?xml version="1.0" encoding="utf-8"?>
<t:stylesheet version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:t="http://www.w3.org/1999/XSL/Transform">
	<t:include href="/templates.xsl" />
	<t:output method="xml" indent="no" media-type="application/xhtml+xml" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" />

	<t:template match="/page">
		<html xml:lang="en">
			<t:call-template name="head">
				<t:with-param name="title">Reprocessing Market</t:with-param>
			</t:call-template>
			<body id="reprocessingMarket">
				<t:call-template name="menu" />
				<div id="content">
					<t:apply-templates />
				</div>
			</body>
		</html>
	</t:template>

	<t:template match="orders">
		<div class="dataWrap">
			<table class="data">
				<caption>Reprocessing Market</caption>
				<thead>
					<tr>
						<th>I</th>
						<th>Type</th>
						<th>Material Price</th>
						<th>Price</th>
						<th>Profit</th>
						<th>H</th>
						<th>Volume</th>
						<th>Quality</th>
					</tr>
				</thead>
				<tbody>
					<t:for-each select="order">
						<tr>
							<td class="icon">
								<a href="#" onclick="CCPEVE.showMarketDetails({@typeID}); return false;">
									<img class="icon" src="http://image.eveonline.com/Type/{@typeID}_32.png" />
								</a>
							</td>
							<td>
								<t:if test="string-length(@orderNo) != 0">
									<t:attribute name="class">blue</t:attribute>
								</t:if>
								<t:value-of select="@typeName" />
							</td>
							<td><t:value-of select="@materialPrice" /></td>
							<td><t:value-of select="@price" /></td>
							<td>
								<t:if test="@profitRatio != ''">
									<span class="{@profitStatus}"><t:value-of select="@profitRatio" />%</span> &#8756; <t:value-of select="@profit" />
								</t:if>
							</td>
							<td class="spark"><img src="/chart/reprocessingProfit.png?typeID={@typeID}" /></td>
							<td><t:value-of select="@volToOrder" /></td>
							<td><t:value-of select="@quality" /></td>
						</tr>
					</t:for-each>
				</tbody>
			</table>
		</div>
		<div class="dataClear"></div>
	</t:template>

</t:stylesheet>
